import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TableMultiplication';

   // Déclaration de la variable (typée !) pour l'utilisateur
 nom:string = '';
 // Fonction retournant un booléen et non le nom
 getConnect() {
 return this.nom != '';
 }

}
